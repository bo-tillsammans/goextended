package goextended

import (
	"errors"
)

var (
	ErrInvalidToken = errors.New("invalid token")
)

type Service string
type Audience Service

const (
	Prefix                       = "goextended"
	IssuerAdService      Service = Prefix + "-iss-ad-service"
	IssuerUserService    Service = Prefix + "-iss-user-service"
	IssuerPaymentService Service = Prefix + "-iss-payment-service"
)
