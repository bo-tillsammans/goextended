package users

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"github.com/valyala/fasthttp"

	"gitlab.com/bo-tillsammans/goextended"
)

func TestFiberMiddlewareSuccess(t *testing.T) {
	signingKey := []byte("super-secret-key")

	users := Users{
		signingKey: signingKey,
		issuer:     goextended.IssuerAdService,
	}

	app := fiber.New()
	app.Get("/", users.FiberMiddleware(), func(ctx *fiber.Ctx) error {
		claims, ok := ctx.Locals(FiberScopeName).(*Claims)
		if !ok {
			t.Fatalf("could not cast local value to claims")
		}
		if len(claims.UIDs) != 1 {
			t.Fatalf("unexpected UIDs length, wanted 1, got: %d", len(claims.UIDs))
		}
		return nil
	})

	claims := Claims{
		UIDs: []string{"uid1"},
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "fiber_middleware_test",
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(1 * time.Hour)),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(signingKey)
	if err != nil {
		t.Fatalf("could not sign token: %v", err)
	}

	req := httptest.NewRequest(fiber.MethodGet, "/", nil)
	cookie := http.Cookie{
		Name:  CookieName,
		Value: ss,
	}
	req.AddCookie(&cookie)
	_, err = app.Test(req, -1)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestFiberMiddlewareErrors(t *testing.T) {
	signingKey := []byte("super-secret-key")

	tests := []struct {
		name          string
		cookieKey     string
		cookieValueFn func(t *testing.T) string
		wantError     *fiber.Error
	}{
		{
			name:      "Without cookie value",
			cookieKey: "foo",
			cookieValueFn: func(_ *testing.T) string {
				return "foo"
			},
			wantError: fiber.NewError(fiber.StatusBadRequest, ErrEmptyCookieValue),
		},
		{
			name:      "With expired token",
			cookieKey: CookieName,
			cookieValueFn: func(t *testing.T) string {
				claims := Claims{
					UIDs: []string{"uid1"},
					RegisteredClaims: jwt.RegisteredClaims{
						Issuer:    "fiber_middleware_test",
						ExpiresAt: jwt.NewNumericDate(time.Now().Add(-1 * time.Hour)),
					},
				}
				token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
				ss, err := token.SignedString(signingKey)
				if err != nil {
					t.Fatalf("could not sign token: %v", err)
				}
				return ss
			},
			wantError: fiber.NewError(fiber.StatusBadRequest, ErrCouldNotParseToken),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			app := fiber.New()
			ctx := app.AcquireCtx(&fasthttp.RequestCtx{})
			defer app.ReleaseCtx(ctx)
			ctx.Request().Header.Set("Cookie", test.cookieKey+"="+test.cookieValueFn(t))
			users := Users{
				signingKey: signingKey,
				issuer:     goextended.IssuerAdService,
			}
			err := users.middleware(ctx)
			if err == nil {
				t.Errorf("expected (%v) error, but no error was thrown", test.wantError)
			}
			var e *fiber.Error
			if !errors.As(err, &e) {
				t.Errorf("expected (%v) error, but got (%v)", test.wantError, err)
			}
			if e.Code != test.wantError.Code {
				t.Errorf("expected (%v) code, but got (%v)", test.wantError.Code, e.Code)
			}
			if e.Message != test.wantError.Message {
				t.Errorf("expected (%v) message, but got (%v)", test.wantError.Message, e.Message)
			}
		})
	}
}
