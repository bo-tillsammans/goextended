package users

import (
	"time"

	"github.com/golang-jwt/jwt/v5"

	"gitlab.com/bo-tillsammans/goextended"
)

const (
	prefix     = goextended.Prefix + "-users"
	CookieName = prefix + "-cookie"
)

type Claims struct {
	UIDs []string `json:"UIDs"`
	jwt.RegisteredClaims
}

func (u *Users) SignedToken(id string, UIDs []string, audience goextended.Audience, expiresAt time.Time) (string, error) {
	aud := jwt.ClaimStrings{string(audience)}
	claims := Claims{
		UIDs: UIDs,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    string(u.issuer),
			Audience:  aud,
			ExpiresAt: jwt.NewNumericDate(expiresAt),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ID:        id,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(u.signingKey)
	if err != nil {
		return "", err
	}
	return ss, nil
}

func (u *Users) ParseToken(tokenString string) (*Claims, error) {
	c := Claims{}
	_, err := jwt.ParseWithClaims(tokenString, &c, func(token *jwt.Token) (interface{}, error) {
		return u.signingKey, nil
	})
	if err != nil {
		return nil, err
	}
	return &c, nil
}

type Users struct {
	signingKey []byte
	issuer     goextended.Service
}

func New(signingKey []byte, issuer goextended.Service) *Users {
	return &Users{
		signingKey: signingKey,
		issuer:     issuer,
	}
}
