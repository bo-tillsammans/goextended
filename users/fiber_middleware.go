package users

import (
	"github.com/gofiber/fiber/v2"
)

var (
	ErrEmptyCookieValue   = "empty cookie value"
	ErrCouldNotParseToken = "could not parse token"
)

const FiberScopeName = "users-extended-claims"

func (u *Users) middleware(c *fiber.Ctx) error {
	usersExtendedToken := c.Cookies(CookieName)
	if usersExtendedToken == "" {
		return fiber.NewError(fiber.StatusBadRequest, ErrEmptyCookieValue)
	}

	claims, err := u.ParseToken(usersExtendedToken)
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, ErrCouldNotParseToken)
	}

	c.Locals(FiberScopeName, claims)

	return c.Next()
}

func (u *Users) FiberMiddleware() func(c *fiber.Ctx) error {
	return u.middleware
}
