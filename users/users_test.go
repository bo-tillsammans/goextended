package users

import (
	"testing"
	"time"

	"gitlab.com/bo-tillsammans/goextended"
)

func TestUsers(t *testing.T) {
	tests := []struct {
		name     string
		id       string
		UIDs     []string
		audience goextended.Audience
	}{
		{
			name:     "asd",
			id:       "asd",
			UIDs:     []string{"foo"},
			audience: "audience",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			users := New([]byte("super-secret-key"), goextended.IssuerAdService)
			ss, err := users.SignedToken(test.id, test.UIDs, test.audience, time.Now().Add(1*time.Hour))
			if err != nil {
				t.Errorf("should not error")
			}
			if ss == "" {
				t.Errorf("got empty string")
			}
			cl, err := users.ParseToken(ss)
			if err != nil {
				t.Errorf("error is not nil - %v", err)
			}
			if len(cl.UIDs) != 1 {
				t.Errorf("should only contain one UID, got: %d", len(cl.UIDs))
			}
			if cl.UIDs[0] != "foo" {
				t.Errorf("first UID should be foo, got: %s", cl.UIDs[0])
			}
		})
	}
}
